import math._
import scala.util._

/**
  * Auto-generated code below aims at helping you parse
  * the standard input according to the problem statement.
  **/
object Player extends App {
  val factorycount = readInt // the number of factories
  val linkcount = readInt // the number of links between factories
  var ls: List[Array[Int]] = Nil
  for(i <- 0 until linkcount) yield {
    val Array(factory1, factory2, distance) = for(i <- readLine split " ") yield i.toInt
    ls :+= Array(factory1, factory2, distance)
  }
  var index = 1
  while(true) {
    var ls_f: Array[Array[Int]] = Array()
    //список фабрик
    var ls_w: Array[Array[Int]] = Array() //список військ

    val entitycount = readInt // the number of entities (e.g. factories and troops)
    for (i <- 0 until entitycount) {
      //input Stream
      val arr = readLine.split(" ") // arr = Array(entityid, entitytype, arg1, arg2, arg3, arg4, arg5)
      arr(1) match {
        case "FACTORY" => ls_f :+= arr.filterNot(p => p.equals("FACTORY")).map(_.toInt)
        case "TROOP" => ls_w :+= arr.filterNot(p => p.equals("TROOP")).map(_.toInt)
      }
    }
    // фільтрація даних по списках
    val ls_f_m = ls_f.filter(x => x(1) == 1)
    //мої фабрики
    val ls_f_e = ls_f.filter(x => x(1) == -1)
    //фабрики ворога
    val ls_f_n = ls_f.filter(x => x(1) == 0)
    //нейтральні фабрики
    val ls_w_m = ls_w.filter(x => x(1) == 1)
    //мої війська
    val ls_w_e = ls_w.filter(x => x(1) == -1) //війська ворога

    //перевіряє скільки в загонах ворога кіборгів якщо у загоні більше ніж index
    //і таких загонів 10% тоді збільшую кількість кіборгів у своїх загонах
    def enemy(count: Int): Boolean = {
      val l = ls_w_e.filter(p => p(4) > count)
      if (ls_f_e.length != 0)
        l.length / ls_f_e.length > 0.1
      else true
    }

    // перевіряє чи війська ворога менші ніж половина моїх військ
    def enemyCout(): Boolean = {
      ls_w_e.length <= ls_w_m.length / 2
    }

    //відсилає війська із фобрики army1 до фабрику army2
    def atak(army1: Array[Array[Int]], army2: Array[Array[Int]]): String = {
      var s = ""
      army1.foreach {
        a =>  army2.foreach {
            v => {
              if (s.length < 1000 && !v.equals(a)) {
                if ((a(3) == 3 && a(2) > 10) || (a(3) == 2 && a(2) > 6) ||
                  (a(3) == 1 && a(2) > 5) || (a(3) < 1 && a(2) > 2))
                  s += "MOVE " + a(0) + " " + v(0) + " " + index + ";"
                else {
                  s += "WAIT;"
                  scala.util.control.Breaks
                }
              } else scala.util.control.Breaks
            }
          }
      }
      s
    }


    if(ls_f_m.isEmpty || ls_f_e.isEmpty) println("WAIT")
    else{
      val sorted_m = ls_f_m.sortWith((a,b) => a(2) < b(2))
      val sorted_e = ls_f_e.sortWith((a,b) => a(2) < b(2))
      val sorted_n = ls_f_n.sortWith((a,b) => a(2) < b(2))
      val sort = ls_f_m.sortWith((a,b) => a(2) > b(2))

      if(ls_w_m.length > ls_w_e.length) index += 1
      else {
        index -= 1
        if (index <= 0) index = 1
      }

      if(enemy(index)) index += 1
      else {
        index -= 1
        if (index <= 0) index = 1
      }
      var s =""
      if (!ls_f_n.isEmpty) {
        s += atak(sorted_m, sorted_n) +
          atak(ls_f_m.sortWith((a,b) => a(2) > b(2)), sorted_n)
      } else {
        if (!enemyCout()) {
          s += atak(sort,sorted_e) + atak(sort, ls_f_m)
        } else {
          s += atak(sort, ls_f_m) + atak(sort,sorted_e)
        }
      }
      println(s.init)
    }
  }
}
